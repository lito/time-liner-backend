package main

import (
	"log"
	"net/http"

	"bitbucket.org/lito/timeliner-backend/controllers"
	"github.com/gorilla/mux"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {
	r := mux.NewRouter()
	for _, route := range controllers.GetRoutes() {
		r.HandleFunc(route.Path, route.Handler).Methods(route.Method)
	}
	log.Fatal(http.ListenAndServe(":8000", r))
}
