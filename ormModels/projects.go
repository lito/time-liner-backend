package ormModels

type Project struct {
	ID       uint `gorm:"primary_key"`
	Name     string
	Color    string
	IsActive bool
	UserID   uint
	Tasks    []*Task
}

func NewProject(name string, userId uint) *Project {
	return &Project{
		Name:     name,
		UserID:   userId,
		IsActive: true,
	}
}
func (project *Project) SetTask(task *Task) {
	project.Tasks = append(project.Tasks, task)
}
