package ormModels

type Task struct {
	ID         uint `gorm:"primary_key"`
	Name       string
	InProgress bool
	ProjectID  uint
}

func NewTask(name string, inProgress bool) *Task {
	return &Task{
		Name:       name,
		InProgress: inProgress,
	}
}
