package ormModels

type User struct {
	ID       uint `gorm:"primary_key"`
	Username string
	Email    string
	Name     string
	Projects []Project
}

func NewUser(name string, username string) *User {
	return &User{
		Name:     name,
		Username: username,
	}
}
