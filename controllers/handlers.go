package controllers

import (
	"errors"
	"net/http"
	"strconv"

	"bitbucket.org/lito/timeliner-backend/ormModels"
	"bitbucket.org/lito/timeliner-backend/utils"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

type Handler struct {
	DB     *gorm.DB
	Params Params
}

func NewHandler(r *http.Request) *Handler {
	params := collectUserParams(r)
	db, err := utils.ConnectDB()

	if err != nil {
		utils.Logger(err)
	}

	db.AutoMigrate(&ormModels.User{}, &ormModels.Project{}, &ormModels.Task{})

	return &Handler{
		Params: params,
		DB:     db,
	}
}

type Params struct {
	Username string
	Name     string
	ID       uint
	IsActive bool
}

func collectUserParams(r *http.Request) Params {
	vars := mux.Vars(r)
	params := Params{
		IsActive: true,
	}
	if vars["ID"] != "" {
		if id, err := strconv.Atoi(vars["ID"]); err == nil {
			params.ID = uint(id)
		}
	}
	if vars["name"] != "" {
		params.Name = vars["name"]
	}
	if username := r.Header.Get("X-Auth-Username"); username != "" {
		params.Username = username
	} else {
		utils.Logger(errors.New("user name required"))
	}
	return params
}

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	rw := NewReponseWriter(w)
	rw.Write("Home")
}

func ProjectsHandler(w http.ResponseWriter, r *http.Request) {
	handler := NewHandler(r)
	db := handler.DB
	defer db.Close()

	var user ormModels.User
	var projects []ormModels.Project
	if err := db.Where(&ormModels.User{Username: handler.Params.Username}).First(&user).Error; err != nil {
		utils.Logger(err)
	}
	db.Model(&ormModels.User{ID: user.ID}).Related(&projects)
	for index, project := range projects {
		var tasks []*ormModels.Task
		db.Model(&ormModels.Project{ID: project.ID}).Related(&tasks)
		projects[index].Tasks = tasks
	}
	rw := NewReponseWriter(w)
	rw.Write(projects)
}

func ProjectHandler(w http.ResponseWriter, r *http.Request) {
	params := collectUserParams(r)
	db, err := utils.ConnectDB()
	defer db.Close()
	if err != nil {
		utils.Logger(err)
	} else {
		db.AutoMigrate(&ormModels.Project{}, &ormModels.Task{})
		var user ormModels.User
		var project ormModels.Project
		var projects []ormModels.Project
		if err := db.Where(&ormModels.User{Username: params.Username}).First(&user).Error; err != nil {
			utils.Logger(err)
		}
		if err := db.Model(&ormModels.User{ID: user.ID}).Related(&projects).Where(&ormModels.Project{ID: params.ID}).First(&project).Error; err != nil {
			utils.Logger(err)
		}

		var tasks []*ormModels.Task

		db.Model(&ormModels.Project{ID: project.ID}).Related(&tasks)

		project.Tasks = tasks
		rw := NewReponseWriter(w)
		rw.Write(project)
	}
}
func CreateProjectHandler(w http.ResponseWriter, r *http.Request) {
	params := collectUserParams(r)
	db, err := utils.ConnectDB()
	defer db.Close()
	if err != nil {
		utils.Logger(err)
	} else {
		db.AutoMigrate(&ormModels.Project{}, &ormModels.Task{})
		var user ormModels.User
		if err := db.Where(&ormModels.User{Username: params.Username}).First(&user).Error; err != nil {
			utils.Logger(err)
		}
		project := ormModels.NewProject("New project", user.ID)
		res := db.Save(&project)
		if res.Error != nil {
			utils.Logger(err)
		}
		rw := NewReponseWriter(w)
		rw.Write(project)
	}
}

func ProjectTasksHandler(w http.ResponseWriter, r *http.Request) {
	params := collectUserParams(r)
	db, err := utils.ConnectDB()
	defer db.Close()
	if err != nil {
		utils.Logger(err)
	} else {
		db.AutoMigrate(&ormModels.Project{}, &ormModels.Task{})
		var tasks []ormModels.Task
		res := db.Model(ormModels.Project{ID: params.ID}).Related(&tasks)
		if res.Error != nil {
			utils.Logger(err)
		}
		rw := NewReponseWriter(w)
		rw.Write(&tasks)
	}
}

func CreateTasksHandler(w http.ResponseWriter, r *http.Request) {
	params := collectUserParams(r)
	db, err := utils.ConnectDB()
	defer db.Close()
	if err != nil {
		utils.Logger(err)
	} else {
		db.AutoMigrate(&ormModels.Project{}, &ormModels.Task{})
		task := ormModels.NewTask(params.Name, params.IsActive)
		var user ormModels.User
		var project ormModels.Project
		var projects []ormModels.Project
		if err := db.Where(&ormModels.User{Username: params.Username}).First(&user).Error; err != nil {
			utils.Logger(err)
		}
		if err := db.Model(&ormModels.User{ID: user.ID}).Related(&projects).Where(&ormModels.Project{ID: params.ID}).First(&project).Error; err != nil {
			utils.Logger(err)
		}
		project.SetTask(task)
		res := db.Save(&project)
		if res.Error != nil {
			utils.Logger(err)
		}

		rw := NewReponseWriter(w)
		rw.Write(res.Value)
	}
}
