package controllers

import (
	"net/http"

	"encoding/json"
)

type ResponseWriter struct {
	writer http.ResponseWriter
}

func NewReponseWriter(writer http.ResponseWriter) *ResponseWriter {
	return &ResponseWriter{
		writer,
	}
}

func (rw *ResponseWriter) writeJSON(response []byte) {
	rw.writer.Header().Set("Content-Type", "application/json; charset=utf-8")
	rw.writer.Write(response)
}

func (rw *ResponseWriter) Write(response interface{}) error {
	resJSON, err := json.Marshal(response)
	if err != nil {
		return err
	}
	rw.writeJSON(resJSON)
	return nil
}
