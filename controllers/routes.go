package controllers

import "net/http"

type Route struct {
	Path    string
	Method  string
	Handler func(w http.ResponseWriter, r *http.Request)
}

func GetRoutes() []Route {
	return []Route{
		{
			Path:    "/",
			Method:  "GET",
			Handler: IndexHandler,
		},
		{
			Path:    "/projects",
			Method:  "GET",
			Handler: ProjectsHandler,
		},
		{
			Path:    "/projects",
			Method:  "POST",
			Handler: CreateProjectHandler,
		},
		{
			Path:    "/projects/{ID}",
			Method:  "GET",
			Handler: ProjectHandler,
		},
		{
			Path:    "/projects/{ID}/tasks",
			Method:  "GET",
			Handler: ProjectTasksHandler,
		},
		{
			Path:    "/projects/{ID}/tasks",
			Method:  "POST",
			Handler: CreateTasksHandler,
		},
	}
}
