package utils

import "github.com/jinzhu/gorm"

import "bitbucket.org/lito/timeliner-backend/config"
import "fmt"

func ConnectDB() (*gorm.DB, error) {
	config := config.DB
	db, err := gorm.Open(
		"mysql",
		fmt.Sprintf("%s:%s@/timeliner?charset=utf8&parseTime=True", config["username"], config["password"]))
	return db, err
}
