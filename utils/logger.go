package utils

import "fmt"

func Logger(err error) {
	fmt.Errorf("Error occurs %s", err.Error())
	panic(err)
}
